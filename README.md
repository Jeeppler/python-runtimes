# Different Python Runtimes

Different Python runtimes in Docker containers. This project can be used to
to play around with:

- CPython
- Cython
- Jython
- MicroPython
- PyPy

### CPython

[CPython](https://github.com/python/cpython) is the standard Python runtime.
The upper case C in front of the CPython is confusing. However, the C simply
indicates that the Python interpreter is written in C.

    # Start cpython container
    docker-compose --file CPython/docker-compose.yaml up --build

    # Shell into the container
    [host]$ docker exec -it cpython bash

    # Run `loop.py`
    [container]# python3 loop.py

    # Using Python2
    [container]# python2 loop.py

### Cython

    # Start cython container
    docker-compose --file Cython/docker-compose.yaml up --build

    # Shell into the container
    [host]$ docker exec -it cython bash

    # Transpile Python to C code
    # The `--embed` generates a `main()` function that embeds the
    # Python interpreter.
    [container]# cython3 --embed -o loop.c loop.py

    # Compile `loop.c` into an executable `loop`
    [container]# gcc -v -Os -I"/usr/include/python3.6m/" -L"/usr/lib64/" -o loop loop.c  -lpython3.6m  -lpthread -lm -lutil -ldl

    # Run the `loop` executable
    [container]# ./loop

### Jython

    # Start jython container
    [host]$ docker-compose --file Jython/docker-compose.yaml up --build

    # Shell into the container
    [host]$ docker exec -it jython bash

    # Run `loop.py`
    [container]# jython loop.py

    # Display Jython and Java version
    [container]# jython --version
    [container]# java -version

### MicroPython

    # Start micropython container
    [host]$ docker-compose --file MicroPython/docker-compose.yaml up --build

    # Shell into the container
    [host]$ docker exec -it micropython bash

    # Run `loop.py`
    [container]# micropython loop.py

### PyPy

    # Start pypy container
    [host]$ docker-compose --file PyPy/docker-compose.yaml up --build

    # Shell into the container
    [host]$ docker exec -it pypy bash

    # Run `loop.py`
    [container]# pypy3 loop.py
